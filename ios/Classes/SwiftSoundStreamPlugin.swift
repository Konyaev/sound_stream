import Flutter
import UIKit
import AVFoundation

import os

public enum SoundStreamErrors: String {
    case FailedToRecord
    case FailedToPlay
    case FailedToStop
    case FailedToWriteBuffer
    case Unknown
}

public enum SoundStreamStatus: String {
    case Unset
    case Initialized
    case Playing
    case Stopped
    case StoppedByOS
}

@available(iOS 9.0, *)
public class SwiftSoundStreamPlugin: NSObject, FlutterPlugin {
    private var channel: FlutterMethodChannel
    private var registrar: FlutterPluginRegistrar
    private var hasPermission: Bool = false
    private var debugLogging: Bool = false

    private var isRecorderInitialized: Bool = false
    private var isReconfiguringAudioEngine: Bool = false
    private var isReconfiguringPlaybackEngine: Bool = false
    private var isRecording: Bool = false
    private var isStoppingRecording: Bool = false

    private var isSuspended: Bool = false
    private var timesRetriedMic: Int = 0

    //========= Recorder's vars
    private var mAudioEngine = AVAudioEngine()
    private let mRecordBus = 0
    private var mInputNode: AVAudioInputNode!
    private var mRecordSampleRate: Double = 24000 // 24Khz
    private var mRecordBufferSize: AVAudioFrameCount = 512
    private var mRecordChannel = 0
    private var mRecordSettings: [String:Int]!
    private var mRecordFormat: AVAudioFormat!
    
    //========= Player's vars
    private var playerAudioEngine = AVAudioEngine()
    private let mPlayerBus = 0
    private var mPlayerNode = AVAudioPlayerNode()
    private var mPlayerSampleRate: Double = 16000 // 16Khz
    private var mPlayerBufferSize: AVAudioFrameCount = 4096
    private var mPlayerOutputFormat: AVAudioFormat!
    private var mPlayerInputFormat: AVAudioFormat!
    private var timePitch = AVAudioUnitTimePitch()
    private var eq: AVAudioUnitEQ!
    
    /** ======== Basic Plugin initialization ======== **/
    
    public static func register(with registrar: FlutterPluginRegistrar) {
        let channel = FlutterMethodChannel(name: "vn.casperpas.sound_stream:methods", binaryMessenger: registrar.messenger())
        let instance = SwiftSoundStreamPlugin( channel, registrar: registrar)
        registrar.addMethodCallDelegate(instance, channel: channel)
    }
    
    init( _ channel: FlutterMethodChannel, registrar: FlutterPluginRegistrar ) {
        self.channel = channel
        self.registrar = registrar
        super.init()
        self.configureAudioSessionForRecording()
        self.attachPlayer()

        NotificationCenter.default.addObserver(
                                                self,
                                                selector: #selector(audioEngineConfigurationChanged(notification:)),
                                                name: .AVAudioEngineConfigurationChange,
                                                object: mAudioEngine)
                                
        NotificationCenter.default.addObserver(self, 
                                               selector: #selector(appDidBecomeActive(notification:)), 
                                               name: UIApplication.didBecomeActiveNotification, 
                                               object: nil)

        // NotificationCenter.default.addObserver(
        //                                         self,
        //                                         selector: #selector(handleInterruption(notification:)),
        //                                         name: AVAudioSession.interruptionNotification,
        //                                         object: AVAudioSession.sharedInstance())

        // NotificationCenter.default.addObserver(self, selector: #selector(appWillEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)

    }

    @objc private func handleInterruption(notification: Notification) {
        os_log("\nhandleInterruption")
        guard let userInfo = notification.userInfo,
            let typeValue = userInfo[AVAudioSessionInterruptionTypeKey] as? UInt,
            let type = AVAudioSession.InterruptionType(rawValue: typeValue) else {
                return
        }
        if type == .began {
            isSuspended = true
            // configureAudioSessionForPlaying()
        } else if type == .ended {
            guard let optionsValue = userInfo[AVAudioSessionInterruptionOptionKey] as? UInt else {
                return
            }
            isSuspended = false
            configureAudioSessionForRecording()
            let options = AVAudioSession.InterruptionOptions(rawValue: optionsValue)
            if options.contains(.shouldResume) {
                os_log("\nhandleInterruption ended + shouldResume")
            }
        }
    }

        
    @objc func appDidBecomeActive(notification: Notification) {
        reconfigureEngine()
        reconfigurePlaybackEngine()
    }

    @objc private func audioEngineConfigurationChanged(notification: Notification) {
        reconfigureEngine()
    }

    private func reconfigureEngine() {
        os_log("\naudioEngineConfigurationChanged before condition")
        if isRecorderInitialized && !isReconfiguringAudioEngine {
            isReconfiguringAudioEngine = true
            os_log("\nWE HERE: audioEngineConfigurationChanged")
            mAudioEngine.stop()
            mAudioEngine.reset()
            mAudioEngine = AVAudioEngine()
            mInputNode = mAudioEngine.inputNode
            configureAudioSessionForRecording()

            NotificationCenter.default.addObserver(self,
                                            selector: #selector(audioEngineConfigurationChanged(notification:)),
                                            name: .AVAudioEngineConfigurationChange,
                                            object: mAudioEngine)
            mAudioEngine.prepare()        
            isReconfiguringAudioEngine = false
        }
    }

    private func reconfigurePlaybackEngine() {
        if !isReconfiguringPlaybackEngine {
            isReconfiguringPlaybackEngine = true
            playerAudioEngine = AVAudioEngine()
            mPlayerNode = AVAudioPlayerNode()
            let speed = timePitch.rate
            timePitch = AVAudioUnitTimePitch()
            timePitch.rate = speed
            configureAudioSessionForRecording()
            attachPlayer()
            isReconfiguringPlaybackEngine = false
        }
    }

    private func configureAudioSessionForRecording(forceConfiguration: Bool = false) -> Bool {
        let audioSession = AVAudioSession.sharedInstance()
        if audioSession.category == AVAudioSession.Category.playAndRecord && !forceConfiguration { return true }
        do {
            try audioSession.setCategory(.playAndRecord, mode: .default, options: [.allowBluetooth, .allowBluetoothA2DP, .defaultToSpeaker, .mixWithOthers])
            try audioSession.setActive(true, options: [])
            isSuspended = false
            return true
        } catch {
            isSuspended = true
            os_log("Failed to activate audio session for recording: %@", log: OSLog.default, type: .error, String(describing: error))
            // configureAudioSessionForPlaying()
            return false
        }
    }

    private func configureAudioSessionForPlaying() {
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(.playback, mode: .default, options: [.allowBluetooth, .allowBluetoothA2DP, .defaultToSpeaker, .mixWithOthers])
            try audioSession.setActive(true, options: [])
        } catch {
            os_log("Failed to activate audio session: %@", log: OSLog.default, type: .error, String(describing: error))
        }
    }
        
    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        switch call.method {
        case "hasPermission":
            hasPermission(result)
        case "initializeRecorder":
            initializeRecorder(call, result)
        case "startRecording":
            startRecording(result)
        case "getRecordingSampleRate":
            getRecordingSampleRate(result)
        case "getPlayerSampleRate":
            getPlayerSampleRate(result)
        case "changePlayerSampleRate":
            changePlayerSampleRate(call, result)
        case "stopRecording":
            stopRecording(result)
        case "initializePlayer":
            initializePlayer(call, result)
        case "startPlayer":
            startPlayer(result)
        case "stopPlayer":
            stopPlayer(result)
        case "writeChunk":
            writeChunk(call, result)
        case "setPlaybackSpeed":
            setPlaybackSpeed(call, result)
        case "getPlaybackSpeed":
            getPlaybackSpeed(result)
        default:
            print("Unrecognized method: \(call.method)")
            sendResult(result, FlutterMethodNotImplemented)
        }
    }
    
    private func sendResult(_ result: @escaping FlutterResult, _ arguments: Any?) {
        DispatchQueue.main.async {
            result( arguments )
        }
    }
    
    private func invokeFlutter( _ method: String, _ arguments: Any? ) {
        DispatchQueue.main.async {
            self.channel.invokeMethod( method, arguments: arguments )
        }
    }
    
    /** ======== Plugin methods ======== **/
    
    private func checkAndRequestPermission(completion callback: @escaping ((Bool) -> Void)) {
        if (hasPermission) {
            callback(hasPermission)
            return
        }
        
        var permission: AVAudioSession.RecordPermission
        #if swift(>=4.2)
        permission = AVAudioSession.sharedInstance().recordPermission
        #else
        permission = AVAudioSession.sharedInstance().recordPermission()
        #endif
        switch permission {
        case .granted:
            print("granted")
            hasPermission = true
            callback(hasPermission)
            break
        case .denied:
            print("denied")
            hasPermission = false
            callback(hasPermission)
            break
        case .undetermined:
            print("undetermined")
            AVAudioSession.sharedInstance().requestRecordPermission() { [unowned self] allowed in
                if allowed {
                    self.hasPermission = true
                    print("undetermined true")
                    callback(self.hasPermission)
                } else {
                    self.hasPermission = false
                    print("undetermined false")
                    callback(self.hasPermission)
                }
            }
            break
        default:
            callback(hasPermission)
            break
        }
    }
    
    private func hasPermission( _ result: @escaping FlutterResult) {
        checkAndRequestPermission { value in
            self.sendResult(result, value)
        }
    }
    
    private func startEngine() {
        guard !mAudioEngine.isRunning else {
            return
        }
        
        try? mAudioEngine.start()
    }
    
    private func stopEngine() {
        mAudioEngine.stop()
        mAudioEngine.reset()
    }
    
    private func sendEventMethod(_ name: String, _ data: Any) {
        var eventData: [String: Any] = [:]
        eventData["name"] = name
        eventData["data"] = data
        invokeFlutter("platformEvent", eventData)
    }
    
    private func initializeRecorder(_ call: FlutterMethodCall, _ result: @escaping FlutterResult) {
        configureAudioSessionForRecording()
        guard let argsArr = call.arguments as? Dictionary<String,AnyObject>
            else {
                sendResult(result, FlutterError( code: SoundStreamErrors.Unknown.rawValue,
                                                 message:"Incorrect parameters, not a dictionary",
                                                 details: nil ))
                return
        }
        mRecordSampleRate = argsArr["sampleRate"] as? Double ?? mRecordSampleRate
        debugLogging = argsArr["showLogs"] as? Bool ?? debugLogging
        mRecordFormat = AVAudioFormat(commonFormat: AVAudioCommonFormat.pcmFormatInt16, sampleRate: mRecordSampleRate, channels: 1, interleaved: true)
        
        checkAndRequestPermission { isGranted in
            if isGranted {
                
            } else {
                self.sendResult(result, FlutterError( code: SoundStreamErrors.Unknown.rawValue,
                                                      message:"No mic permission",
                                                      details: nil ))
                return
            }
        }
        isRecorderInitialized = true
        mInputNode = mAudioEngine.inputNode
        mAudioEngine.prepare()
        self.sendRecorderStatus(SoundStreamStatus.Initialized)
        self.sendResult(result, true)
    }
    
    private func resetEngineForRecord() -> Bool {
        mAudioEngine.inputNode.removeTap(onBus: mRecordBus)        
        let input = mAudioEngine.inputNode
        let inputFormat = input.outputFormat(forBus: mRecordBus)
        let converter = AVAudioConverter(from: inputFormat, to: mRecordFormat!)!
        let ratio: Float = Float(inputFormat.sampleRate)/Float(mRecordFormat.sampleRate)

        let realInputFormat = input.inputFormat(forBus: mRecordBus)
        // for debug:
        // let outputFormatDescription = String(describing: inputFormat)
        // os_log("Output format: %@", outputFormatDescription)
        // let inputFormatDescription = String(describing: realInputFormat)
        // os_log("Input format: %@", inputFormatDescription)

        guard realInputFormat.sampleRate == inputFormat.sampleRate && inputFormat.sampleRate != 0 else {
            os_log("resetEngineForRecord relaunch")
            let outputFormatDescription = String(describing: inputFormat)
            os_log("Output format: %@", outputFormatDescription)
            let inputFormatDescription = String(describing: realInputFormat)
            os_log("Input format: %@", inputFormatDescription)

            if inputFormat.sampleRate == 0 && timesRetriedMic > 10 && !isReconfiguringAudioEngine {
                self.sendRecorderStatus(SoundStreamStatus.StoppedByOS)
                return false
            }
            timesRetriedMic += 1

            try Thread.sleep(forTimeInterval: 0.1) // Wait for 0.1 seconds
            self.resetEngineForRecord()
            return true
        }
        timesRetriedMic = 0

        //my code
        let adjustedBufferSize = ratio * Float(mRecordBufferSize)
        let actualBufferSize = UInt32(adjustedBufferSize.rounded())
        //end my code
        
        input.installTap(onBus: mRecordBus, bufferSize: actualBufferSize, format: inputFormat) { (buffer, time) -> Void in
            self.isSuspended = false

            let inputCallback: AVAudioConverterInputBlock = { inNumPackets, outStatus in
                outStatus.pointee = .haveData
                return buffer
            }
            
            let convertedBuffer = AVAudioPCMBuffer(pcmFormat: self.mRecordFormat!, frameCapacity: UInt32(Float(buffer.frameCapacity) / ratio))!
            
            var error: NSError?
            let status = converter.convert(to: convertedBuffer, error: &error, withInputFrom: inputCallback)
            assert(status != .error)
            
            if (self.mRecordFormat?.commonFormat == AVAudioCommonFormat.pcmFormatInt16) {
                let values = self.audioBufferToBytes(convertedBuffer)
                self.sendMicData(values)
            }

            // to not loose what's left in the buffer after the user's stopped recording
            if self.isStoppingRecording {
                // Process and send the last buffer to Flutter
                let values = self.audioBufferToBytes(convertedBuffer)
                self.sendMicData(values)
                // Now remove the tap and reset flags
                DispatchQueue.main.async {
                    self.mAudioEngine.inputNode.removeTap(onBus: self.mRecordBus)
                    self.isRecording = false
                    self.isStoppingRecording = false // Reset the flag
                    self.sendRecorderStatus(SoundStreamStatus.Stopped)
                }
            }
        }
        return true
    }
    
    private func startRecording(_ result: @escaping FlutterResult) {
        guard !isReconfiguringAudioEngine else {
            os_log("startRecording relaunch")
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.startRecording(result)
            }
            return
        }
        var sessionReadyForRecording: Bool = configureAudioSessionForRecording()
        if sessionReadyForRecording == false {
            result(false)
            return            
        }
        var successfullReset: Bool = resetEngineForRecord()
        if successfullReset == false {
            result(false)
            return            
        }
        isRecording = true
        startEngine()
        sendRecorderStatus(SoundStreamStatus.Playing)
        result(true)
    }

    private func getRecordingSampleRate(_ result: @escaping FlutterResult) {
        if let inputNode = mInputNode {
            let inputFormat = inputNode.outputFormat(forBus: mRecordBus)
            let actualSampleRate = inputFormat.sampleRate
            result(actualSampleRate)
        } else {
            result(FlutterError(code: SoundStreamErrors.Unknown.rawValue, 
                                message: "Input node is not available", 
                                details: nil))
        }
    }
    
    private func stopRecording(_ result: @escaping FlutterResult) {
        guard !isReconfiguringAudioEngine else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.stopRecording(result)
            }
            return
        }
        if isRecording == false {
            return
        }
        isStoppingRecording = true
        // mAudioEngine.inputNode.removeTap(onBus: mRecordBus)
        // isRecording = false
        // sendRecorderStatus(SoundStreamStatus.Stopped)
        result(true)
    }
    
    private func sendMicData(_ data: [UInt8]) {
        let channelData = FlutterStandardTypedData(bytes: NSData(bytes: data, length: data.count) as Data)
        sendEventMethod("dataPeriod", channelData)
    }
    
    private func sendRecorderStatus(_ status: SoundStreamStatus) {
        sendEventMethod("recorderStatus", status.rawValue)
    }
    
    private func initializePlayer(_ call: FlutterMethodCall, _ result: @escaping FlutterResult) {
        guard let argsArr = call.arguments as? Dictionary<String,AnyObject>
            else {
                sendResult(result, FlutterError( code: SoundStreamErrors.Unknown.rawValue,
                                                 message:"Incorrect parameters",
                                                 details: nil ))
                return
        }
        mPlayerSampleRate = argsArr["sampleRate"] as? Double ?? mPlayerSampleRate
        debugLogging = argsArr["showLogs"] as? Bool ?? debugLogging
        mPlayerInputFormat = AVAudioFormat(commonFormat: AVAudioCommonFormat.pcmFormatInt16, sampleRate: mPlayerSampleRate, channels: 1, interleaved: true)
        sendPlayerStatus(SoundStreamStatus.Initialized)
        sendResult(result, true)
    }
    
    private func attachPlayer() {
        mPlayerOutputFormat = AVAudioFormat(commonFormat: AVAudioCommonFormat.pcmFormatFloat32, sampleRate: mPlayerSampleRate, channels: 1, interleaved: true)
        
        playerAudioEngine.attach(mPlayerNode)
        playerAudioEngine.attach(timePitch)
        timePitch.pitch = 75.0
        
        // Initialize the EQ with two bands for high-pass and low-pass filters
        eq = AVAudioUnitEQ(numberOfBands: 2)
        
        // High-Pass Filter to remove low frequencies (e.g., below 150Hz)
        let highPassBand = eq.bands[0]
        highPassBand.filterType = .highPass
        highPassBand.frequency = 150.0    // Set the high-pass cutoff frequency
        highPassBand.bandwidth = 0.5      // Adjust bandwidth
        highPassBand.bypass = false       // Ensure it's active
        
        // Low-Pass Filter to remove high frequencies (e.g., above 4000Hz)
        let lowPassBand = eq.bands[1]
        lowPassBand.filterType = .lowPass
        lowPassBand.frequency = 6000.0    // Set the low-pass cutoff frequency
        lowPassBand.bandwidth = 0.5       // Adjust bandwidth
        lowPassBand.bypass = false        // Ensure it's active
        
        playerAudioEngine.attach(eq)

        // Connect the player node to the EQ, then the EQ to the timePitch, and finally to the output
        playerAudioEngine.connect(mPlayerNode, to: eq, format: mPlayerOutputFormat)
        playerAudioEngine.connect(eq, to: timePitch, format: mPlayerOutputFormat)
        playerAudioEngine.connect(timePitch, to: playerAudioEngine.outputNode, format: mPlayerOutputFormat)

        try? playerAudioEngine.prepare()
    }

    private func changePlayerSampleRate(_ call: FlutterMethodCall, _ result: @escaping FlutterResult) {
        guard let argsArr = call.arguments as? Dictionary<String,AnyObject>
            else {
                sendResult(result, FlutterError( code: SoundStreamErrors.Unknown.rawValue,
                                                 message:"Incorrect parameters",
                                                 details: nil ))
                return
        }
        mPlayerSampleRate = argsArr["sampleRate"] as? Double ?? mPlayerSampleRate
        mPlayerInputFormat = AVAudioFormat(commonFormat: AVAudioCommonFormat.pcmFormatInt16, sampleRate: mPlayerSampleRate, channels: 1, interleaved: true)
        reconfigurePlaybackEngine()
        sendResult(result, mPlayerSampleRate)
    }

    private func getPlayerSampleRate(_ result: @escaping FlutterResult) {
        sendResult(result, mPlayerSampleRate)
    }
    
    private func startPlayer(_ result: @escaping FlutterResult) {
        guard !isReconfiguringPlaybackEngine else {
            os_log("startPlayer relaunch")
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.startPlayer(result)
            }
            return
        }
        let isOtherAudioPlaying = AVAudioSession.sharedInstance().isOtherAudioPlaying
        if isOtherAudioPlaying {
            configureAudioSessionForPlaying()
        }
        if !playerAudioEngine.isRunning  {
            do {
                try playerAudioEngine.start()
            } catch {
                result(false)
                return
            } 
        }
        if !mPlayerNode.isPlaying {
            mPlayerNode.play()
        }
        result(true)
    }
    
    private func stopPlayer(_ result: @escaping FlutterResult) {
        if mPlayerNode.isPlaying {
            mPlayerNode.stop()
        }
        mPlayerNode.reset()
        sendPlayerStatus(SoundStreamStatus.Stopped)
        result(true)
    }
    
    private func sendPlayerStatus(_ status: SoundStreamStatus) {
        sendEventMethod("playerStatus", status.rawValue)
    }
    
    private func writeChunk(_ call: FlutterMethodCall, _ result: @escaping FlutterResult) {
        guard let argsArr = call.arguments as? Dictionary<String,AnyObject>,
            let data = argsArr["data"] as? FlutterStandardTypedData
            else {
                sendResult(result, FlutterError( code: SoundStreamErrors.FailedToWriteBuffer.rawValue,
                                                 message:"Failed to write Player buffer",
                                                 details: nil ))
                return
        }
        let byteData = [UInt8](data.data)
        pushPlayerChunk(byteData, result)
    }

    private func setPlaybackSpeed(_ call: FlutterMethodCall, _ result: @escaping FlutterResult) {
         guard let argsArr = call.arguments as? Dictionary<String,Any>,
            let speed = argsArr["speed"] as? Double
            else {
                sendResult(result, FlutterError( code: SoundStreamErrors.FailedToWriteBuffer.rawValue,
                                                 message:"Speed not specified",
                                                 details: nil ))
                return
        }

        timePitch.rate = Float(speed)
        sendResult(result, true)
    }

    private func mutePlayer(_ result: @escaping FlutterResult) {
        mPlayerNode.volume = 0.0
        result(true)
    }

    private func unmutePlayer(_ result: @escaping FlutterResult) {
        mPlayerNode.volume = 1.0
        result(true)
    }

    private func getPlaybackSpeed(_ result: @escaping FlutterResult) {
        let speed = timePitch.rate
        result(Double(speed))
    }

    private func applyFadeToBuffer(_ buffer: AVAudioPCMBuffer) -> AVAudioPCMBuffer {
        let fadeDuration: AVAudioFrameCount = 128 // Adjust as needed
        let fadeIn = AVAudioFrameCount(buffer.frameLength) < fadeDuration ? AVAudioFrameCount(buffer.frameLength) : fadeDuration
        let fadeOut = AVAudioFrameCount(buffer.frameLength) < fadeDuration ? AVAudioFrameCount(buffer.frameLength) : fadeDuration
        
        let frameCount = buffer.frameLength
        
        for frame in 0..<fadeIn {
            let gain = Float(frame) / Float(fadeIn)
            buffer.floatChannelData?.pointee[Int(frame)] *= gain
        }
        
        for frame in (0..<fadeOut).reversed() {
            let gain = Float(frame) / Float(fadeOut)
            buffer.floatChannelData?.pointee[Int(frameCount) - Int(frame) - 1] *= gain
        }
        
        return buffer
    }
    
    private func pushPlayerChunk(_ chunk: [UInt8], _ result: @escaping FlutterResult) {
        let buffer = bytesToAudioBuffer(chunk)
        let processedBuffer = applyFadeToBuffer(buffer)
        mPlayerNode.scheduleBuffer(convertBufferFormat(
            buffer,
            from: mPlayerInputFormat,
            to: mPlayerOutputFormat
        ));
        result(true)
    }
    
    private func convertBufferFormat(_ buffer: AVAudioPCMBuffer, from: AVAudioFormat, to: AVAudioFormat) -> AVAudioPCMBuffer {
        
        let formatConverter =  AVAudioConverter(from: from, to: to)
        let ratio: Float = Float(from.sampleRate)/Float(to.sampleRate)
        let pcmBuffer = AVAudioPCMBuffer(pcmFormat: to, frameCapacity: UInt32(Float(buffer.frameCapacity) / ratio))!
        
        var error: NSError? = nil
        let inputBlock: AVAudioConverterInputBlock = {inNumPackets, outStatus in
            outStatus.pointee = .haveData
            return buffer
        }
        formatConverter?.convert(to: pcmBuffer, error: &error, withInputFrom: inputBlock)
        
        return pcmBuffer
    }
    
    private func audioBufferToBytes(_ audioBuffer: AVAudioPCMBuffer) -> [UInt8] {
        let srcLeft = audioBuffer.int16ChannelData![0]
        let bytesPerFrame = audioBuffer.format.streamDescription.pointee.mBytesPerFrame
        let numBytes = Int(bytesPerFrame * audioBuffer.frameLength)
        
        // initialize bytes by 0
        var audioByteArray = [UInt8](repeating: 0, count: numBytes)
        
        srcLeft.withMemoryRebound(to: UInt8.self, capacity: numBytes) { srcByteData in
            audioByteArray.withUnsafeMutableBufferPointer {
                $0.baseAddress!.initialize(from: srcByteData, count: numBytes)
            }
        }
        
        return audioByteArray
    }
    
    private func bytesToAudioBuffer(_ buf: [UInt8]) -> AVAudioPCMBuffer {
        let frameLength = UInt32(buf.count) / mPlayerInputFormat.streamDescription.pointee.mBytesPerFrame
        
        let audioBuffer = AVAudioPCMBuffer(pcmFormat: mPlayerInputFormat, frameCapacity: frameLength)!
        audioBuffer.frameLength = frameLength
        
        let dstLeft = audioBuffer.int16ChannelData![0]
        
        buf.withUnsafeBufferPointer {
            let src = UnsafeRawPointer($0.baseAddress!).bindMemory(to: Int16.self, capacity: Int(frameLength))
            dstLeft.initialize(from: src, count: Int(frameLength))
        }
        
        return audioBuffer
    }

}
