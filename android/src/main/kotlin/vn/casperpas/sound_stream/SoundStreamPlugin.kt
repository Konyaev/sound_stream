package vn.casperpas.sound_stream

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Application
import android.os.Bundle
import android.content.Context
import android.content.pm.PackageManager
import android.content.BroadcastReceiver
import android.content.IntentFilter
import android.content.Intent
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothClass
import android.media.*
import android.media.AudioRecord.OnRecordPositionUpdateListener
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.BinaryMessenger
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry
import io.flutter.plugin.common.PluginRegistry.Registrar
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.ShortBuffer
import java.lang.ref.WeakReference;
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel

const val methodChannelName = "vn.casperpas.sound_stream:methods"

enum class SoundStreamErrors {
    FailedToRecord,
    FailedToPlay,
    FailedToStop,
    FailedToWriteBuffer,
    Unknown,
}

enum class SoundStreamStatus {
    Unset,
    Initialized,
    Playing,
    Stopped,
}

/** SoundStreamPlugin */
public class SoundStreamPlugin : FlutterPlugin,
    MethodCallHandler,
    PluginRegistry.RequestPermissionsResultListener,
    ActivityAware,
    PluginRegistry.ActivityResultListener {
    private val logTag = "SoundStreamPlugin"
    private val audioRecordPermissionCode = 14887

    private lateinit var methodChannel: MethodChannel
    private var currentActivity: Activity? = null
    private var pluginContext: Context? = null
    private var permissionToRecordAudio: Boolean = false
    private var activeResult: Result? = null
    private var debugLogging: Boolean = false

    //========= Recorder's vars
    private val mRecordFormat = AudioFormat.ENCODING_PCM_16BIT
    private var mRecordSampleRate = 16000 // 16Khz
    private var mRecorderBufferSize = 512
    private var mPeriodFrames = 512
    private var audioData: ShortArray? = null
    private var mRecorder: AudioRecord? = null
    private var mListener: OnRecordPositionUpdateListener? = null

    //========= Player's vars
    private var mAudioTrack: AudioTrack? = null
    private var mAudioManager: AudioManager? = null
    private var mPlayerSampleRate = 16000 // 16Khz
    private var mPlayerBufferSize =  4096 // 32768
    private var mPlayerFormat: AudioFormat = AudioFormat.Builder()
        .setEncoding(AudioFormat.ENCODING_PCM_16BIT)
        .setChannelMask(AudioFormat.CHANNEL_OUT_MONO)
        .setSampleRate(mPlayerSampleRate)
        .build()
    private var playbackParams: PlaybackParams? = null

    private val coroutineScope = CoroutineScope(Dispatchers.Default + SupervisorJob())
    private val audioTrackLock = Any()

    private val requestEnableBT = 1

    private val chunkQueue = Channel<ShortArray>(Channel.UNLIMITED)

    private val chunkProcessorJob = coroutineScope.launch {
        for (chunk in chunkQueue) {
            // Process each chunk
            synchronized(audioTrackLock) {
                mAudioTrack?.write(chunk, 0, chunk.size)
            }
        }
    }
     
    /** ======== Basic Plugin initialization ======== **/

    override fun onAttachedToEngine(flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
        pluginContext = flutterPluginBinding.getApplicationContext()
        methodChannel = MethodChannel(flutterPluginBinding.getBinaryMessenger(), methodChannelName)
        methodChannel.setMethodCallHandler(this)
    }

    override fun onMethodCall(call: MethodCall, result: Result) {
        try {
            when (call.method) {
                "hasPermission" -> hasPermission(result)
                "initializeRecorder" -> initializeRecorder(call, result)
                "usePhoneSpeaker" -> usePhoneSpeaker(call, result)
                "startRecording" -> startRecording(result)
                "stopRecording" -> stopRecording(result)
                "initializePlayer" -> initializePlayer(call, result)
                "setPlaybackSpeed" -> setPlaybackSpeed(call, result)
                "getPlaybackSpeed" -> getPlaybackSpeed(result)
                "changePlayerSampleRate" -> changePlayerSampleRate(call, result)
                "startPlayer" -> startPlayer(result)
                "stopPlayer" -> stopPlayer(result)
                "writeChunk" -> writeChunk(call, result)
                else -> result.notImplemented()
            }
        } catch (e: Exception) {
            Log.e(logTag, "Unexpected exception", e)
            result.error(
                SoundStreamErrors.Unknown.name,
                "Unexpected exception", e.localizedMessage
            )
        }
    }

    override fun onDetachedFromEngine(binding: FlutterPlugin.FlutterPluginBinding) {
        try {
            methodChannel.setMethodCallHandler(null)
            mListener?.onMarkerReached(null)
            mListener?.onPeriodicNotification(null)
            mListener = null
            mRecorder?.let {
                it.stop()
                it.release()
            }
            mRecorder = null
        } catch (e: Exception) {
            debugLog("onDetachedFromEngine error: ${e.localizedMessage}")
        }
    }

    override fun onDetachedFromActivity() {
        debugLog("real onDetachedFromActivity")
        unregisterBluetoothReceiver()
    }

    override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {
        debugLog("real onReattachedToActivityForConfigChanges")
        currentActivity = binding.getActivity()
        val callbacks = LifecycleCallbacks(this, binding.getActivity().hashCode())
        binding.getActivity().getApplication().registerActivityLifecycleCallbacks(callbacks)
        binding.getActivity().setVolumeControlStream(AudioManager.STREAM_MUSIC)
        binding.addRequestPermissionsResultListener(this)
        ensureBluetoothIsEnabled()
    }

    override fun onAttachedToActivity(binding: ActivityPluginBinding) {
        debugLog("real onAttachedToActivity")
        currentActivity = binding.getActivity()
        val callbacks = LifecycleCallbacks(this, binding.getActivity().hashCode())
        binding.getActivity().getApplication().registerActivityLifecycleCallbacks(callbacks)
        binding.getActivity().setVolumeControlStream(AudioManager.STREAM_MUSIC)
        binding.addRequestPermissionsResultListener(this)
        binding.addActivityResultListener(this)
        registerBluetoothReceiver()
        ensureBluetoothIsEnabled()
    }

    override fun onDetachedFromActivityForConfigChanges() {
        debugLog("real override fun onDetachedFromActivityForConfigChanges")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?): Boolean {
        if (requestCode == requestEnableBT) {
            if (resultCode == Activity.RESULT_OK) {
                // Bluetooth has been enabled by the user
                Log.d(logTag, "Bluetooth has been enabled.")
            } else {
                // User did not enable Bluetooth or an error occurred
                Log.d(logTag, "Failed to enable Bluetooth.")
            }
            return true
        }
        return false
    }

    private fun ensureBluetoothIsEnabled() {
        val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        if (bluetoothAdapter != null && !bluetoothAdapter.isEnabled) {
            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            currentActivity?.startActivityForResult(enableBtIntent, requestEnableBT)
        }
    }

    private val bluetoothReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            initAudioManager()
            val action = intent.action
            if (BluetoothDevice.ACTION_ACL_CONNECTED == action) {
                // Device has just connected
                mAudioManager?.setBluetoothScoOn(true)
                mAudioManager?.startBluetoothSco()
                Log.d(logTag, "Bluetooth device connected, switching audio output.")
            } else if (BluetoothDevice.ACTION_ACL_DISCONNECTED == action) {
                // Device has just disconnected
                mAudioManager?.setBluetoothScoOn(false)
                mAudioManager?.stopBluetoothSco()
                Log.d(logTag, "Bluetooth device disconnected, reverting audio output.")
            }
        }
    }

    private fun registerBluetoothReceiver() {
        val filter = IntentFilter().apply {
            addAction(BluetoothDevice.ACTION_ACL_CONNECTED)
            addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED)
        }
        currentActivity?.registerReceiver(bluetoothReceiver, filter)
    }

    private fun unregisterBluetoothReceiver() {
        currentActivity?.unregisterReceiver(bluetoothReceiver)
    }

    private fun initAudioManager() {
        if (mAudioManager != null) return
        mAudioManager = (currentActivity?.getSystemService(Context.AUDIO_SERVICE) as? AudioManager)?.apply {
            requestAudioFocus(AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN).run {
                setAudioAttributes(AudioAttributes.Builder().run {
                    setLegacyStreamType(AudioManager.STREAM_MUSIC)
                    setUsage(AudioAttributes.USAGE_MEDIA)
                    setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    build()
                })
                build()
            })
        }
    }

    /** ======== Plugin methods ======== **/

    private fun hasRecordPermission(): Boolean {
        if (permissionToRecordAudio) return true

        val localContext = pluginContext
        permissionToRecordAudio = localContext != null && ContextCompat.checkSelfPermission(
            localContext,
            Manifest.permission.RECORD_AUDIO
        ) == PackageManager.PERMISSION_GRANTED
        return permissionToRecordAudio

    }

    private fun hasPermission(result: Result) {
        result.success(hasRecordPermission())
    }

    private fun requestRecordPermission() {
        val localActivity = currentActivity
        if (!hasRecordPermission() && localActivity != null) {
            debugLog("requesting RECORD_AUDIO permission")
            ActivityCompat.requestPermissions(
                localActivity,
                arrayOf(Manifest.permission.RECORD_AUDIO), audioRecordPermissionCode
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<out String>,
        grantResults: IntArray
    ): Boolean {
        when (requestCode) {
            audioRecordPermissionCode -> {
                permissionToRecordAudio = grantResults.isNotEmpty() &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED
                completeInitializeRecorder()
                return true
            }
        }
        return false
    }

    private fun initializeRecorder(call: MethodCall, result: Result) {
        initAudioManager()
        mRecordSampleRate = call.argument<Int>("sampleRate") ?: mRecordSampleRate
        debugLogging = call.argument<Boolean>("showLogs") ?: false
        mPeriodFrames = AudioRecord.getMinBufferSize(
            mRecordSampleRate,
            AudioFormat.CHANNEL_IN_MONO,
            mRecordFormat
        )
        mRecorderBufferSize = mPeriodFrames * 2
        audioData = ShortArray(mPeriodFrames)
        activeResult = result

        val localContext = pluginContext
        if (null == localContext) {
            completeInitializeRecorder()
            return
        }
        permissionToRecordAudio = ContextCompat.checkSelfPermission(
            localContext,
            Manifest.permission.RECORD_AUDIO
        ) == PackageManager.PERMISSION_GRANTED
        if (!permissionToRecordAudio) {
            requestRecordPermission()
        } else {
            debugLog("has permission, completing")
            completeInitializeRecorder()
        }
        debugLog("leaving initializeIfPermitted")
        result.success(true)
    }

    @SuppressLint("MissingPermission")
    private fun initRecorder() {
        if (mRecorder?.state == AudioRecord.STATE_INITIALIZED) {
            return
        }

        mRecorder = AudioRecord(
            MediaRecorder.AudioSource.MIC,
            mRecordSampleRate,
            AudioFormat.CHANNEL_IN_MONO,
            mRecordFormat,
            mRecorderBufferSize
        )
        mListener = createRecordListener()
        mRecorder?.positionNotificationPeriod = mPeriodFrames
        mRecorder?.setRecordPositionUpdateListener(mListener)
    }

    private fun completeInitializeRecorder() {

        debugLog("completeInitialize")
        val initResult: HashMap<String, Any> = HashMap()

        if (permissionToRecordAudio) {
            mRecorder?.release()
            initRecorder()
            initResult["isMeteringEnabled"] = true
            sendRecorderStatus(SoundStreamStatus.Initialized)
        }

        initResult["success"] = permissionToRecordAudio
        debugLog("sending result")
        activeResult?.success(initResult)
        debugLog("leaving complete")
        activeResult = null
    }

    private fun sendEventMethod(name: String, data: Any) {
        val eventData: HashMap<String, Any> = HashMap()
        eventData["name"] = name
        eventData["data"] = data
        methodChannel.invokeMethod("platformEvent", eventData)
    }

    private fun debugLog(msg: String) {
        if (debugLogging) {
            Log.d(logTag, msg)
        }
    }

    private fun startRecording(result: Result) {
        try {
            if (mRecorder!!.recordingState == AudioRecord.RECORDSTATE_RECORDING) {
                result.success(true)
                return
            }
            initRecorder()
            mRecorder!!.startRecording()
            sendRecorderStatus(SoundStreamStatus.Playing)
            result.success(true)
        } catch (e: IllegalStateException) {
            debugLog("record() failed")
            result.error(
                SoundStreamErrors.FailedToRecord.name,
                "Failed to start recording",
                e.localizedMessage
            )
        }
    }

    private fun stopRecording(result: Result) {
        try {
            if (mRecorder!!.recordingState == AudioRecord.RECORDSTATE_STOPPED) {
                result.success(true)
                return
            }
            mRecorder!!.stop()
            sendRecorderStatus(SoundStreamStatus.Stopped)
            result.success(true)
        } catch (e: IllegalStateException) {
            debugLog("record() failed")
            result.error(
                SoundStreamErrors.FailedToRecord.name,
                "Failed to start recording",
                e.localizedMessage
            )
        }
    }

    private fun sendRecorderStatus(status: SoundStreamStatus) {
        sendEventMethod("recorderStatus", status.name)
    }

    private fun initializePlayer(call: MethodCall, result: Result) {
        initAudioManager()
        mPlayerSampleRate = call.argument<Int>("sampleRate") ?: mPlayerSampleRate
        debugLogging = call.argument<Boolean>("showLogs") ?: false
        mPlayerFormat = AudioFormat.Builder()
            .setEncoding(AudioFormat.ENCODING_PCM_16BIT)
            .setChannelMask(AudioFormat.CHANNEL_OUT_MONO)
            .setSampleRate(mPlayerSampleRate)
            .build()

        mPlayerBufferSize = AudioTrack.getMinBufferSize(
            mPlayerSampleRate,
            AudioFormat.CHANNEL_OUT_MONO,
            AudioFormat.ENCODING_PCM_16BIT
        )

        if (mAudioTrack?.state == AudioTrack.STATE_INITIALIZED) {
            mAudioTrack?.release()
        }

        val audioAttributes = AudioAttributes.Builder()
            .setLegacyStreamType(AudioManager.STREAM_MUSIC)
            .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
            .setUsage(AudioAttributes.USAGE_MEDIA)
            .build()

        mAudioTrack = AudioTrack(
            audioAttributes,
            mPlayerFormat,
            mPlayerBufferSize,
            AudioTrack.MODE_STREAM,
            AudioManager.AUDIO_SESSION_ID_GENERATE
        )

        currentActivity?.setVolumeControlStream(AudioManager.STREAM_MUSIC)

        mAudioManager?.mode = AudioManager.MODE_NORMAL

        result.success(true)
        sendPlayerStatus(SoundStreamStatus.Initialized)
    }

    private fun getPlaybackSpeed(result: Result) {
        val localAudioTrack = mAudioTrack
        if (localAudioTrack != null && localAudioTrack.state == AudioTrack.STATE_INITIALIZED) {
            val currentPlaybackParams = localAudioTrack.playbackParams
            val speed = currentPlaybackParams.speed
            result.success(speed)
        } else {
            result.success(1.0)
        }
    }

    private fun changePlayerSampleRate(call: MethodCall, result: Result) {
        val newSampleRate = call.argument<Number>("sampleRate")?.toInt() ?: mPlayerSampleRate

        if (newSampleRate == mPlayerSampleRate) {
            // No change needed
            result.success(true)
            return 
        }

        mPlayerSampleRate = newSampleRate

        mPlayerFormat = AudioFormat.Builder()
            .setEncoding(AudioFormat.ENCODING_PCM_16BIT)
            .setChannelMask(AudioFormat.CHANNEL_OUT_MONO)
            .setSampleRate(mPlayerSampleRate)
            .build()

        mPlayerBufferSize = AudioTrack.getMinBufferSize(
            mPlayerSampleRate,
            AudioFormat.CHANNEL_OUT_MONO,
            AudioFormat.ENCODING_PCM_16BIT
        )

        val audioAttributes = AudioAttributes.Builder()
            .setLegacyStreamType(AudioManager.STREAM_MUSIC)
            .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
            .setUsage(AudioAttributes.USAGE_MEDIA)
            .build()

        mAudioTrack = AudioTrack(
            audioAttributes,
            mPlayerFormat,
            mPlayerBufferSize,
            AudioTrack.MODE_STREAM,
            AudioManager.AUDIO_SESSION_ID_GENERATE
        )

        result.success(true)
    }

    private fun setPlaybackSpeed(call: MethodCall, result: Result) {
        val localAudioTrack = mAudioTrack
        if (localAudioTrack != null && localAudioTrack.state == AudioTrack.STATE_INITIALIZED) {
            val localPlaybackParams = playbackParams ?: PlaybackParams().also {
                playbackParams = it
            }
            localPlaybackParams.speed = call.argument<Double>("speed")?.toFloat() ?: 1.0f
            localAudioTrack.playbackParams = localPlaybackParams
            result.success(true)
        } else {
            result.error("UNINITIALIZED", "AudioTrack is not initialized", null)
        }
        result.success(true)
    }

    private fun writeChunk(call: MethodCall, result: Result) {
        val data = call.argument<ByteArray>("data")
        if (data != null) {
            pushPlayerChunk(data, result)
        } else {
            result.error(
                SoundStreamErrors.FailedToWriteBuffer.name,
                "Failed to write Player buffer",
                "'data' is null"
            )
        }
        result.success(true)
    }

    private fun pushPlayerChunk(chunk: ByteArray, result: Result) {
        try {
            val buffer = ByteBuffer.wrap(chunk)
            val shortBuffer = ShortBuffer.allocate(chunk.size / 2)
            shortBuffer.put(buffer.order(ByteOrder.LITTLE_ENDIAN).asShortBuffer())
            val shortChunk = shortBuffer.array()
            coroutineScope.launch {
                chunkQueue.send(shortChunk)
            }
        } catch (e: Exception) {
        }
    }

    private fun usePhoneSpeaker(call: MethodCall, result: Result) {
        val useSpeaker = call.argument<Boolean>("value") ?: false
        mAudioManager?.mode = if (useSpeaker) AudioManager.MODE_IN_COMMUNICATION else AudioManager.MODE_NORMAL
        result.success(true)
    }

    private fun startPlayer(result: Result) {
        try {
            if (mAudioTrack?.state == AudioTrack.PLAYSTATE_PLAYING) {
                result.success(true)
                return
            }

            mAudioTrack!!.play()
            sendPlayerStatus(SoundStreamStatus.Playing)
            result.success(true)
        } catch (e: Exception) {
            result.error(
                SoundStreamErrors.FailedToPlay.name,
                "Failed to start Player",
                e.localizedMessage
            )
        }
    }

    private fun stopPlayer(result: Result) {
        try {
            if (mAudioTrack?.state == AudioTrack.STATE_INITIALIZED) {
                mAudioTrack?.stop()
            }
            sendPlayerStatus(SoundStreamStatus.Stopped)
            result.success(true)
        } catch (e: Exception) {
            result.error(
                SoundStreamErrors.FailedToStop.name,
                "Failed to stop Player",
                e.localizedMessage
            )
        }
    }

    private fun sendPlayerStatus(status: SoundStreamStatus) {
        sendEventMethod("playerStatus", status.name)
    }

    private fun createRecordListener(): OnRecordPositionUpdateListener {
        return object : OnRecordPositionUpdateListener {
            override fun onMarkerReached(recorder: AudioRecord) {
                recorder.read(audioData!!, 0, mRecorderBufferSize)
            }

            override fun onPeriodicNotification(recorder: AudioRecord) {
                val data = audioData!!
                val shortOut = recorder.read(data, 0, mPeriodFrames)
                // this condition to prevent app crash from happening in Android Devices
                // See issues: https://github.com/CasperPas/flutter-sound-stream/issues/25
                if (shortOut < 1) {
                    return
                }
                // https://flutter.io/platform-channels/#codec
                // convert short to int because of platform-channel's limitation
                val byteBuffer = ByteBuffer.allocate(shortOut * 2)
                byteBuffer.order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().put(data)

                sendEventMethod("dataPeriod", byteBuffer.array())
            }
        }
    }
}

private class LifecycleCallbacks(
    plugin: SoundStreamPlugin,
    private val activityHashCode: Int
) : Application.ActivityLifecycleCallbacks {
    private val pluginRef: WeakReference<SoundStreamPlugin> = WeakReference(plugin)

    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
        Log.i(TAG, "LifecycleCallbacks.onActivityCreated")
        // Never called, since Activity is already created before plugin.
    }

    override fun onActivityStarted(activity: Activity) {
        Log.i(TAG, "LifecycleCallbacks.onActivityStarted")
        if (activity.hashCode() != activityHashCode) return
    }

    override fun onActivityResumed(activity: Activity) {
        Log.i(TAG, "LifecycleCallbacks.onActivityResumed")
        activity.setVolumeControlStream(AudioManager.STREAM_MUSIC)
        if (activity.hashCode() != activityHashCode) return
        activity.setVolumeControlStream(AudioManager.STREAM_MUSIC)
    }

    override fun onActivityPaused(activity: Activity) {
        // Handle activity pause event if needed
    }

    override fun onActivityStopped(activity: Activity) {
        Log.i(TAG, "LifecycleCallbacks.onActivityStopped")
        if (activity.hashCode() != activityHashCode) return
    }

    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {
        // Handle save instance state if needed
    }

    override fun onActivityDestroyed(activity: Activity) {
        Log.i(TAG, "LifecycleCallbacks.onActivityDestroyed")
        if (activity.hashCode() != activityHashCode) return
        activity.application.unregisterActivityLifecycleCallbacks(this)
    }
}

private const val TAG = "SoundStreamPlugin"

